
#!/bin/bash
# Tool to perform OpenShift cluster installation and future revision control of same.

REPO_ROOT="$(git rev-parse --show-toplevel)"
. $REPO_ROOT/bin/install-common.sh

verify_config_dir

echo "Installiing cluster '$CLUSTER_NAME' from '$INSTALLER_CONFIG_BASE'."


verify_provider

echo ""
echo "Press enter to continue:"
read

# interactive command to perform installation
time "$INSTALLER_BINARY" create cluster --dir="$INSTALLER_CONFIG_BASE" --log-level=debug
