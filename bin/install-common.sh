#!/bin/bash
# Install common

# Nightly builds:
#  https://mirror.openshift.com/pub/openshift-v4/clients/ocp-dev-preview/latest-4.5/
REPO_ROOT="$(git rev-parse --show-toplevel)"
INSTALLER_ROOT="$REPO_ROOT/clusters"
export INSTALLER_BINARY=~/Downloads/openshift-install

PROVIDER=aws
ENVIRONMENT="${1:-test}"
export CLUSTER_NAME="${PROVIDER}-${ENVIRONMENT}"
export INSTALLER_CONFIG_BASE="${INSTALLER_ROOT}/${CLUSTER_NAME}"

verify_cluster_name() {
    if [ ${#CLUSTER_NAME} -gt 14 ]; then
        echo "ERROR: Cluster name '$CLUSTER_NAME' exceeds 14 characters in length"
        exit 1;
    fi
}

verify_config_dir() {
    if [ ! -d "${INSTALLER_CONFIG_BASE}" ]; then
        mkdir -p  "${INSTALLER_CONFIG_BASE}"
    fi
}

verify_provider() {
    if [ "$PROVIDER" == "aws" ]; then
        if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" ]; then
            echo "Consider setting AWS credential environment variables."
        fi
    elif [ "$PROVIDER" == "osp" ]; then
        which openstack > /dev/null
        if [ $? -ne 0 ]; then
            echo "If using 'osp' provider, be sure to have OpenStack client activated, and"
            echo "also ensure a floating IP is available."
        fi
    fi
}