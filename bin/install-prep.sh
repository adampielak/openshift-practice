
#!/bin/bash
# Tool for establishing configuration environment sufficient to drive
# OpenShift cluster installation and future revision control of same.

REPO_ROOT="$(git rev-parse --show-toplevel)"
. $REPO_ROOT/bin/install-common.sh

verify_cluster_name
verify_config_dir

echo "Initializing an installation configuration for '$ENVIRONMENT' environment on '$PROVIDER' infrastructure provider."
echo "Storing installation configs in '$INSTALLER_CONFIG_BASE'."

verify_provider

echo ""
echo "Press enter to continue:"
read

# interactive command to initialize an installation configuration
"$INSTALLER_BINARY" create install-config --log-level=debug --dir="$INSTALLER_CONFIG_BASE"
echo ""
echo "Press enter to continue:"
read

# This will rm the install-config.yaml prematurely
#"$INSTALLER_BINARY" create ignition-configs --dir="$INSTALLER_CONFIG_BASE"
#echo "Ignition configs dumped to '${INSTALLER_CONFIG_BASE}/*.ign'"
