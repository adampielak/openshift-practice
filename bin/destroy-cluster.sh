#!/bin/bash
# Tool for destroying OpenShift cluster installations.

REPO_ROOT="$(git rev-parse --show-toplevel)"
. $REPO_ROOT/bin/install-common.sh

verify_config_dir

echo "Destroying an cluster installation using configs in '$INSTALLER_CONFIG_BASE'."

verify_provider

echo ""
echo "Press enter to continue:"
read

# interactive command to initialize an installation configuration
time "$INSTALLER_BINARY" destroy cluster --dir="$INSTALLER_CONFIG_BASE"
