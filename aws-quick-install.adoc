:toc:
:icons: font
:sectanchors:
:sectlinks:
// doing walkthrough of this
:ocp-ver: 4.3
:install-doc-title: Installing a cluster quickly on AWS
:install-doc-url: https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-default.html
:install-doc-link: {install-doc-url}[{install-doc-title}]
// reference material
:install-update-link: https://docs.openshift.com/container-platform/{ocp-ver}/architecture/architecture-installation.html#architecture-installation[Installation and update]
// provider links
:provider-quota-link: https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html[AWS service quotas]
:provider-dns-link: https://console.aws.amazon.com/route53/[Route53]
:provider-access-key-link: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html[Managing Access Keys for IAM Users]
//
:toc[]:

= OpenShift AWS Quick Install

Example run through of {install-doc-link} on Mac OS X Catalina.

.**Working Directory**
[IMPORTANT]
It is expected you are working from the root directory of this repo.

== Prereqs

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html[Configure an AWS account to host the cluster.]

=== Provider DNS Delegation

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-route53_installing-aws-account[Configuring Route53]

* [x] Create subdomain in {provider-dns-link}, https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html[HowTo]
* [x] Create Hosted Zone: aws.tofu.org, type=public
[source,bash]
$ dig +short soa aws.tofu.org. @ns-1105.awsdns-10.org
ns-1626.awsdns-11.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400

* [x] Delegate subdomain in DNS provider
[source,bash]
$ dig +short ns aws.tofu.org.
ns-1105.awsdns-10.org.
ns-1626.awsdns-11.co.uk.
ns-368.awsdns-46.com.
ns-708.awsdns-24.net.

=== Provider Credentials

* [x] Create or select a https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-iam-user_installing-aws-account[non-root IAM user]
* [x] Grant Programmatic access
* [x] Attach the AdministratorAccess policy
* [x] Upload ssh key
* [x] Record the access key ID and secret access key values. You must use these values when you configure your local machine to run the installation program. {provider-access-key-link}

[TIP]
====
Use standard https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html[Environment Variables To Configure the AWS CLI]

[source,bash]
----
cat /Volumes/Keybase/private/dlbewley/credz/aws/openshift-tigerteam/dbewley.sh | cut -d= -f1
# https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html
#Account: openshift-tigerteam
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION

source /Volumes/Keybase/private/dlbewley/credz/aws/openshift-tigerteam/dbewley.sh
----
====

=== Provider Limits

* [x] Verify that account https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html[AWS service quotas] offer https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-limits_installing-aws-account[adequate quotas]

* [x] Verify account has https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-permissions_installing-aws-account[adequate permissions]

=== Internet and Telemetry access for OpenShift Container Platform
=== Generating an SSH private key and adding it to the agent

== Installation
=== Obtaining the installation program

* [x] Download and save pull secret

[source,bash]
$ export PULL_SECRET=/Volumes/Keybase/private/dlbewley/credz/redhat/pull-secret/dbewley@redhat.com.json

- [x] Download the installer 

[source,bash]
curl -O https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-mac.tar.gz
tar xvzf openshift-install-mac.tar.gz openshift-install
alias openshift-install=$(pwd)/openshift-install

- [x] https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac[Enable execution] of the `openshift-install` binary. Ctrl-click in finder and open to bypass OSX signature check or use `spctl`.

[source,bash]
spctl --add --label "OpenShift" openshift-install

=== Deploy the cluster

- [x] Run the installer

[source,bash]
export CLUSTER_DIR=$(pwd)/clusters/aws-quick
time openshift-install create cluster --dir="$CLUSTER_DIR" --log-level=info 
# 40:33 total

== Logging in to the cluster

=== Installing the CLI by downloading the binary

- [x] Download the `oc` client

[source,bash]
curl -O https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-mac.tar.gz
tar xvzf openshift-client-mac.tar.gz kubectl oc

- [x] Permit execution of the binaries

[source,bash]
spctl --add --label "OpenShift" oc
spctl --add --label "OpenShift" kubectl

- [x] Pass credentials to `oc` via kubeconfig and login.

[source,bash]
cp clusters/aws-quick/auth/kubeconfig /Volumes/Keybase/private/dlbewley/credz/ocp/aws-quick/kuebconfig
export KUBECONFIG=/Volumes/Keybase/private/dlbewley/credz/ocp/aws-quick/kuebconfig
alias oc=$(pwd)/oc
alias kubectl=$(pwd)/kubectl

[source,bash]
$ oc version
Client Version: 4.3.12
Server Version: 4.3.12
Kubernetes Version: v1.16.2
$ oc get nodes
NAME                                         STATUS   ROLES    AGE   VERSION
ip-10-0-130-207.us-west-1.compute.internal   Ready    master   61m   v1.16.2
ip-10-0-131-17.us-west-1.compute.internal    Ready    worker   52m   v1.16.2
ip-10-0-135-56.us-west-1.compute.internal    Ready    worker   52m   v1.16.2
ip-10-0-140-219.us-west-1.compute.internal   Ready    master   61m   v1.16.2
ip-10-0-151-26.us-west-1.compute.internal    Ready    master   61m   v1.16.2
ip-10-0-155-125.us-west-1.compute.internal   Ready    worker   52m   v1.16.2


== Uninstallation
=== Destroy the Cluster

- [ ] https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/uninstalling-cluster-aws.html[Destroy the cluster] when you are done.

[source,bash]
time openshift-install destroy cluster --dir=$CLUSTER_DIR --log-level=info